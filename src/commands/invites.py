import datetime
import typing
import aiohttp
import discord


class InviteManager:
    def __init__(self):
        self.initial_invites = {}
        self.website_invites = set()

    async def update_invites(self, guild):
        invites = await guild.invites()
        for invite in invites:
            self.initial_invites[invite.code] = invite.uses

        async with aiohttp.ClientSession() as ses:
            res = await ses.get("https://catancommunity.org/invites/all_invites")
            res.raise_for_status()
            self.website_invites.update(await res.json())

    def parse_time(self, time_string):
        if time_string == "[not set]":
            return time_string

        time = datetime.datetime.fromisoformat(time_string)
        return f"<t:{int(time.timestamp())}>"

    async def on_user_join(
        self,
        guild: discord.Guild,
        member: discord.Member,
        spamChannel: discord.TextChannel,
        db,
    ):
        invites: typing.List[discord.Invite] = await guild.invites()
        for invite in invites:
            if self.initial_invites.get(invite.code, 0) != invite.uses:
                if invite.code in self.website_invites:
                    async with aiohttp.ClientSession() as ses:
                        res = await ses.get(
                            "https://catancommunity.org/invites/get_referrer/"
                            + invite.code
                        )
                        user_session_data = await res.json()
                    await spamChannel.send(
                        f"User **{member}** joined using a invite made by **{invite.inviter}**."
                        f"Recently, someone visited the website got shown this invite. They *may* or *may not* be the same person:\n"
                        f"Referrer: {user_session_data.get('referrer', '[not set]')!r}\n"
                        f"First page visited: {user_session_data.get('first_page', '[not set]')!r}\n"
                        f"Last page visited: {user_session_data.get('last_page', '[not set]')!r}\n"
                        f"First seen: {self.parse_time(user_session_data.get('first_time', '[not set]'))}\n"
                        f"Last time: {self.parse_time(user_session_data.get('last_time', '[not set]'))}\n"
                        "See <https://gitlab.com/catan-community/catan-community-website-3/-/blob/master/referer_bot_message_info.md> for more info"
                    )
                else:
                    await spamChannel.send(
                        f"User **{member}** joined using a invite made by **{invite.inviter}** (Total uses: {invite.uses})"
                    )
                await db["user_invites"].insert_one(
                    {"user_id": member.id, "inviter_id": invite.inviter.id}
                )
                self.initial_invites[invite.code] = invite.uses
                return

    async def get_invites(self, state, message):
        invites = await message.guild.invites()
        if len(invites) == 0:
            await message.channel.send("No active invites found")
        else:
            await message.channel.send(f"Found {len(invites)} active invites")
            msg = ""
            for invite in invites:
                if invite.uses >= 1:
                    msg += f"Invite by `{invite.inviter}` used by `{invite.uses}` people, code=`{invite.code}`\n"
                    if msg.count("\n") >= 6:
                        await message.channel.send(msg)
                        msg = ""
            if len(msg) > 0:
                await message.channel.send(msg)
