import asyncio
import random
import re
import traceback

import discord

import functions
import id_generator
from constants import (
    NO_RECENT_MATCH,
    game_creation_synonyms,
    game_mode_emoji_mapping,
    game_mode_synonyms,
)

match_win_template = """
Congratulations, {name}. You have won {score} {emoji} LP.
Well played, {other_players}. {lp_loss_message}
Type `!leaderboard` to see the top players, or `!rank` in <#{bot_spam_channel}> to see your current rank.
"""


async def create_game(state, message):
    if isinstance(message.channel, discord.DMChannel):
        await message.channel.send("You cannot use this command via direct message")
        return
    words = state.split_words(message.content)
    if len(words) < 2 or not re.match(r"\d+", words[1]):
        words.insert(1, "0")

    if len(words) < 3:
        link = ""
    elif words[2].startswith("http"):
        link = "\n<" + words[2] + ">"
    elif words[2].startswith("<http") and words[2].endswith(">"):
        link = "\n" + words[2]
    else:
        link = ""

    if len(words) == 1:
        wager = 0
    else:
        wager = words[1]
        if not re.match(r"^\d+$", wager):
            await message.channel.send("The wager must be a number")
            return
        wager = int(wager)

        if wager > state.settings["max_wager"]:
            await message.channel.send(
                f"You may wager only up to {state.settings['max_wager']}"
            )
            return

        if wager != 0 and wager < state.settings["min_wager"]:
            await message.channel.send(
                f"Minimal non-zero wager is {repr(state.settings['min_wager'])}"
            )
            return

        if wager % state.settings["wager_step"] != 0:
            await message.channel.send(
                f"Wager must be a multiple of {state.settings['wager_step']}"
            )
            return

    game_modes = set(game_mode_synonyms.keys())
    game_creation_modes = set(game_creation_synonyms.keys())
    words_set = {i.lower() for i in words}
    mentioned_game_modes = {
        game_mode_synonyms[i] for i in game_modes.intersection(words_set)
    }
    mentioned_game_creation_mode = {
        game_creation_synonyms[i] for i in game_creation_modes.intersection(words_set)
    }
    if len(mentioned_game_modes) == 0:
        if len(mentioned_game_creation_mode) == 0:
            await message.channel.send(
                "You must mention at least one game mode in the message: One of "
                + ", ".join(f"`{i}`" for i in game_mode_emoji_mapping.keys())
            )
            return
        else:
            game_mode = next(iter(mentioned_game_creation_mode))

    elif len(mentioned_game_modes) >= 2 or (
        len(mentioned_game_modes) == 1
        and len(mentioned_game_creation_mode) != 0
        and mentioned_game_creation_mode != mentioned_game_modes
    ):
        await message.channel.send(
            "You must include only one game mode text in your message. You included: \n"
            f"{mentioned_game_modes}\n"
            "If you need to include another, consider"
            " prefixing it with plus. For example:\n"
            "> `!creategame fast base +C&K above`\n"
            "This works because C&K is now not it's own word, so the bot knows it's not the real mode."
        )
        return
    else:
        game_mode = next(iter(mentioned_game_modes))
    game_id = id_generator.generate_id()
    game_id_length = len(str(game_id))
    colonist_game_id = (
        str(game_id)[0:4] + str(game_id)[game_id_length - 4 : game_id_length]
    )

    if link == "" and message.content.find("http") == -1:
        link = "\n<https://colonist.io/#" + colonist_game_id + ">"
    await state.create_game(
        message.author, wager, link=link, game_mode=game_mode, game_display_id=game_id
    )

    message = await message.channel.send(
        state.format_game_message(
            game_id, wager, link, [message.author.mention], mode=game_mode
        )
    )
    await message.add_reaction("🎮")


async def start(state, message):
    words = state.split_words(message.content)
    if len(words) == 1:
        game_id = (
            await state.get_player_by_id(
                state.normalize_mention(message.author.mention), {"last_game_id": True}
            )
            or {}
        ).get("last_game_id")
        if game_id is None:
            await message.channel.send(
                "You have no recent games. Try typing the game id explicitly"
            )
            return
        else:
            await message.channel.send(
                f"The last game you participated in was `{game_id}`"
            )
    elif len(words) == 2:
        game_id = words[1]
    else:
        await message.channel.send(
            f"Usage: `!start [game id]` Lock a game to prevent more players from joining."
        )
        return

    if not state.is_valid_game_id(game_id):
        await message.channel.send("Are you sure you typed the game id correctly?")
        return
    game = await state.get_game_by_id(game_id)

    if game is None:
        await message.channel.send(f"There is no game with that id")
        return

    if game.get("started", False):
        await message.channel.send(f"This game has already started")
        return

    if state.normalize_mention(message.author.mention) not in game[
        "players"
    ] and not state.is_admin(message.author):
        await message.channel.send(f"You can only start games you are part of")
        return

    if len(game.get("players")) < 2:
        await message.channel.send("You must play with at least 2 players")
        return

    state.db["matches"].update_one({"_id": game["_id"]}, {"$set": {"started": True}})

    await message.channel.send(
        "The game has started, no further players can join.\n"
        f"Use `!winner {game['id']} @winner` in #report-winner when a player wins"
    )


async def winner(state, message):
    if isinstance(message.channel, discord.DMChannel):
        await message.channel.send("You cannot use this command via direct message")
        return

    words = state.split_words(message.content)
    if len(words) == 2:
        game_id = (
            await state.get_player_by_id(
                state.normalize_mention(message.author.mention), {"last_game_id": True}
            )
        ).get("last_game_id")
        if game_id is None or game_id == NO_RECENT_MATCH:
            await message.channel.send(
                "Can not determine the last game you participated in"
            )
            return

        await message.channel.send(
            f"The last game you participated in was `{game_id}`."
        )
    elif len(words) == 3:
        game_id = words[1]

        if not state.is_valid_game_id(game_id):
            await message.channel.send(
                "Please double check if you typed the game id correctly."
            )
            return
    else:
        await message.channel.send(
            "usage: `!winner [game id] [winner mention]`\n"
            "One player must confirm the win before points are awarded."
        )
        return

    if len(message.mentions) == 0:
        await message.channel.send("You must @mention the winner")
        return

    if len(message.attachments) == 0:
        await message.channel.send("Please remember to also post a screenshot")
        # No return here, message is valid

    match = await state.get_game_by_id(game_id)
    if not match:
        await message.channel.send("Double check your game id: `" + game_id + "`")
        return
    if match["winner"] is not None:
        await message.channel.send(
            f"This match is already pending a win by {match['winner']}, if this was an error"
            f" please contact a moderator"
        )
        return

    winner = state.normalize_mention(words[-1])
    if winner in ["@everyone", "@here"] or len(message.role_mentions) > 0:
        await message.channel.send("nice try")
        return
    elif state.normalize_mention(message.author.mention) not in match["players"]:
        await message.channel.send(
            "You can only submit the results for a match you are a part of"
        )
        return
    elif winner not in match["players"]:
        await message.channel.send(
            winner
            + " did not play in match "
            + repr(game_id)
            + ", make sure the match id and player id are properly spelled, and that "
            + winner
            + " properly reacted to the original message"
        )
        return
    elif len(message.mentions) != 1:
        await message.channel.send("Please tag exactly one winner")
        return
    if len(match["players"]) == 1:
        await message.channel.send(
            message.author.mention + " You cannot play a match with only one player."
        )
        return
    if match.get("admin_cancelled", False):
        await message.channel.send("This match has been cancelled by a mod")
        return

    if (
        match.get("wager", 0) > state.settings.get("max_wager_2p", float("inf"))
        and len(match["players"]) <= 2
    ):
        await message.channel.send(
            "Note: Wager has been capped at "
            + str(state.settings.get("max_wager_2p", 0))
        )

    new_message = await message.channel.send(
        ", ".join(match["players"])
        + ", please react with :thumbsup: to confirm the result of match "
        + functions.game_mode_emoji_mapping[match.get("mode", "base")]
        + " `"
        + game_id
        + "`"
        + "\nTo flag this match for moderator attention, please react with 🛡️"
    )

    await state.db["matches"].update_one(
        {"_id": match["_id"]},
        {
            "$push": {"approved_players": message.author.mention},
            "$set": {
                "winner": winner,
                "winner_display_name": state.get_user_display_name(message.mentions[0]),
                "winner_message_url": message.jump_url,
            },
        },
    )

    await new_message.add_reaction("👍")
    await new_message.add_reaction("🛡️")

    state.message_ids[new_message.id] = match["_id"]


def get_send_message_function(message):
    async def send_message_function(text, **kwargs):
        await asyncio.sleep(random.random() * 2)
        await message.channel.send(text, **kwargs)

    return send_message_function


async def admin_winner(state, message):
    if not state.is_admin(message.author):
        message.channel.send(
            "You do not have permission to perform this action. "
            "Make sure your roles are listed in adminroles."
        )
        return
    words = message.content.split(" ")
    if len(words) != 3:
        await message.channel.send(
            "usage: `!adminwinner [game id] [winner mention]`\n"
            "Immediately make a player win the game without the approval of all players."
        )
        return
    game_id = words[1]
    if not state.is_valid_game_id(game_id):
        await message.channel.send(
            "Please double check if you typed the game id correctly."
        )
        return

    match = await state.get_game_by_id(game_id)
    if match["approved"] or match["mod_approved"]:
        await message.channel.send(
            "This match has already been completed, use !adminCancel to cancel the result of the match first."
        )
        return

    winner = state.normalize_mention(words[2])

    if len(match["players"]) == 1:
        await message.channel.send(
            message.author.mention + " A match with only one player can not be won."
        )
        return
    if winner not in match["players"]:
        await message.channel.send(
            winner
            + " did not play in match "
            + repr(game_id)
            + ", make sure the match id and player id are properly spelled, and that "
            + winner
            + " properly reacted to the original message"
        )
        return

    if len(message.mentions) != 1:
        await message.channel.send("Please tag exactly one winner")
        return

    total_lp_wagered = await state.win_match(
        match,
        winner,
        winner_discord_user=message.mentions[0],
        message="match "
        + match["id"]
        + " (Approved by "
        + state.get_user_display_name(message.author)
        + ")",
        admin_approved=True,
        send_message_function=get_send_message_function(message),
    )

    await message.channel.send(
        match_win_template.format(
            name=winner,
            score=total_lp_wagered,
            other_players=",".join(i for i in match["players"] if i != winner),
            lp_loss_message="",
            emoji=functions.game_mode_emoji_mapping[match.get("mode", "base")],
            bot_spam_channel=state.settings.get("bot_spam_channel", 5),
        )
    )


async def admin_cancel(state, message):
    if not state.is_admin(message.author):
        await message.channel.send(
            message.author.mention + ", you are not a admin and cannot use this command"
        )
        return
    words = message.content.split()
    if len(words) != 2:
        await message.channel.send(
            "usage: !admincancel [match id]\nType !help for more info"
        )
        return
    match_id = words[1]
    if not state.is_valid_game_id(match_id):
        await message.channel.send("That does not appear to be a valid match id")
        return

    match = await state.get_game_by_id(match_id)
    if match is None:
        await message.channel.send("There is no game with that id")
        return

    if match.get("admin_cancelled", False):
        await message.channel.send("This match has already been cancelled")
        return

    lp_subtracted = await state.cancel_match(match)

    await message.channel.send("The match " + match_id + "  has been cancelled.")

    if lp_subtracted:
        additional_message = ""
        if match["wager"] > 0:
            additional_message = (
                ". " + str(match["wager"]) + " has been added to the other player/s."
            )
        await message.channel.send(
            str(match["total_lp_wagered"] - match["wager"])
            + " has been subtracted from "
            + str(match["winner"])
            + additional_message
        )
    else:
        await message.channel.send(
            "However, the match was not completed so no lp was subtracted"
        )


async def confirm_game(state, reaction: discord.Reaction, user: discord.Member):
    words = reaction.message.content.split()
    match_index = words.index("match") + 2
    game_id = words[match_index][1:-1]
    if not state.is_valid_game_id(game_id):
        await user.send(
            "It seems like I got a little confused, and wasn't able to register your approval. Please"
            " contact a moderator to get your score sorted."
        )
        return

    match = await state.get_game_by_id(game_id)
    if state.normalize_mention(user.mention) not in match["players"]:
        await user.send(
            "You can only approve of a match you were a player in."
            " Mods can additionally use `!adminwinner` to immediately approve a match."
        )
        return
    elif user.mention in match["approved_players"]:
        return
    if match.get("admin_cancelled", False):
        await user.send(
            "The match you are trying to approve has been cancelled by a mod."
        )
        return

    match["approved_players"].append(user.mention)
    await state.db["matches"].update_one(
        {"_id": match["_id"]}, {"$addToSet": {"approved_players": user.mention}}
    )
    if (
        len(match["approved_players"]) >= 2
    ):  # The winner and at least one other person have to confirm
        await state.db["matches"].update_one(
            {"_id": match["_id"]}, {"$set": {"approved": True}}
        )

        if match["approved"] or match.get("admin_approved", False):
            await reaction.message.edit(
                content="This match has already been manually approved by an admin. Congratulations!"
            )
            return

        total_lp_wagered = await state.win_match(
            match,
            match["winner"],
            winner_discord_user=discord.utils.get(
                reaction.message.guild.members, mention=match["winner"]
            ),
            message="match " + match["id"],
            send_message_function=get_send_message_function(reaction.message),
        )

        await reaction.message.edit(
            content=match_win_template.format(
                name=match["winner"],
                score=total_lp_wagered,
                other_players=",".join(
                    i for i in match["players"] if i != match["winner"]
                ),
                lp_loss_message="",
                bot_spam_channel=state.settings.get("bot_spam_channel", 5),
                emoji=functions.game_mode_emoji_mapping[match.get("mode", "base")],
                id=match["_id"],
            )
        )


async def my_games_to_confirm(state, message):

    game_check = await state.db["matches"].find_one(
        {"winner": {"$type": 2}, "players": state.normalize_mention(message.author.mention),
         "approved": False, "reported": {"$exists": False}}
    )
    games = state.db["matches"].find(
        {"winner": {"$type": 2}, "players": state.normalize_mention(message.author.mention),
         "approved": False, "reported": {"$exists": False}}
    )

    if game_check is not None:
        await message.channel.send(
            "Games you have played in that need confirmation:"
        )
    else:
        await message.channel.send(
            "Thanks from the CC Team. You have no games that need confirmation"
        )
    async for game in games:
        try:
            match_winner = await state.get_player_by_id(game['winner'], {"display_name": 1})
            winner_name = match_winner['display_name']
            msg_to_send = f"game `{game['id']}` by `{winner_name}`\n" \
                          f"{game.get('winner_message_url', 'no link available')}"
            await message.channel.send(msg_to_send)
        except Exception:
            traceback.print_exc()
            await message.channel.send(f"Error getting data for game `{game['id']}`")


async def calculate_net_wager(state, message, words):
    if len(words) > 2:
        await message.channel.send("Usage: !netwager [@user]")
        return
    if len(words) == 1:
        user = message.author
    else:
        if not len(message.mentions) == 1:
            await message.channel.send("You must mention a user")
            return
        user = message.mentions[0]
    wager_matches = []
    wager_matches = (
        await state.db["matches"]
            .find(
            {
                "$and": [
                    {"players": {"$in": [state.normalize_mention(user.mention)]}},
                    {"wager": {"$ne": 0}},
                    {"winner": {"$ne": None}},
                ]
            },
            {"wager": 1, "winner": 1, "players": 1},
        ).to_list(99999)
    )

    if len(wager_matches) == 0:
        await message.channel.send(
            "This player has not wagered any league points"
        )
        return

    net_profit_or_loss = 0
    total_wagered_league_points = 0

    for match in wager_matches:
        total_wagered_league_points += match["wager"]
        if match["winner"] == user.mention:
            net_profit_or_loss += match["wager"] * (
                    len(match["players"]) - 1
            )
        else:
            net_profit_or_loss -= match["wager"]

    if net_profit_or_loss > 3000:
        profit_or_loss_statement = (
            f"and has an INSANE net profit of {net_profit_or_loss}! "
            f"Congratulations - but please remember sharing is caring"
        )
    elif net_profit_or_loss > 0:
        profit_or_loss_statement = f"and has a net profit of {net_profit_or_loss} ! Hooray, keep it up!"
    elif 0 > net_profit_or_loss >= -400:
        profit_or_loss_statement = (
            f"and has a net loss of {net_profit_or_loss} ! Uh-oh, don't worry "
            f"there's always more points to be won!"
        )
    elif net_profit_or_loss < -400:
        profit_or_loss_statement = (
            f"and has a net loss of {net_profit_or_loss} ! Maybe the player should stick to non-wager games."
        )
    else:
        profit_or_loss_statement = (
            "and has broken even! I'm not sure whether you should be happy or sad."
        )

    await message.channel.send(
        f"{user.mention} has wagered a total of {total_wagered_league_points} LPs over {len(wager_matches)} games"
        f" {profit_or_loss_statement}"
    )