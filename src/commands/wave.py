import io
import typing
import PIL.Image as image
import PIL.ImageSequence as imageSequence
import math
import random
import discord
import aiohttp


async def wave(state, message: discord.Message):
    channel: discord.TextChannel = message.channel

    img_url: typing.Optional[discord.Attachment] = None
    async for message in channel.history(oldest_first=False):
        if len(message.embeds) > 0 and message.embeds[0].image:
            img_url = message.embeds[0].image
            break
        if len(message.attachments) > 0 and message.attachments[
            0
        ].content_type.startswith("image/"):
            img_url = message.attachments[0].url
            break

    if img_url is None:
        await message.channel.send("Could not find a image")
        return

    async with aiohttp.ClientSession(raise_for_status=True) as ses:
        res = await ses.get(img_url)
        img_data = await res.content.read()

    waved_image = wave_image(img_data)

    await channel.send(
        "here you go",
        file=discord.File(waved_image, img_url.replace("/", "-")[-5:] + ".gif"),
    )


def wave_image(img_data: bytes) -> io.BytesIO:
    STEPS = 20
    NROF_FRAMES = 12

    speed = random.randrange(1, 5)
    frequency = random.randrange(1, 3) * 2
    color = tuple(random.randint(0, 255) for i in range(3))
    margin = random.randrange(0, 40)

    curve = (
        lambda x, y: 0.128
        + x
        * math.sin(y * frequency * math.pi / NROF_FRAMES - x * (0.5 + speed) * math.pi)
        / 8
    )
    width_scale = lambda y: 1 - 0.128 * curve(1, y)
    img = image.open(io.BytesIO(img_data))

    source_image_frames = img.n_frames if hasattr(img, "n_frames") else 1
    images = [
        img.seek(frame * source_image_frames // NROF_FRAMES)
        or img.convert("RGBA").transform(
            (512, 512),
            image.Transform.MESH,
            [
                (
                    (
                        int(
                            margin + i * (512 - margin * 2) * width_scale(frame) / STEPS
                        ),
                        0,
                        int(
                            margin
                            + (i + 1) * (512 - margin * 2) * width_scale(frame) // STEPS
                        ),
                        512,
                    ),
                    (
                        i / STEPS * img.width,
                        -img.height * curve(i / STEPS, frame),
                        i / STEPS * img.width,
                        img.height + img.height * curve(i / STEPS, frame),
                        (i + 1) / STEPS * img.width,
                        img.height + img.height * curve((i + 1) / STEPS, frame),
                        (i + 1) / STEPS * img.width,
                        -img.height * curve((i + 1) / STEPS, frame),
                    ),
                )
                for i in range(STEPS)
            ],
            fillcolor=color,
        )
        for frame in range(NROF_FRAMES)
    ]

    output = io.BytesIO()
    images[0].save(
        output,
        format="gif",
        save_all=True,
        append_images=images[1:],
        duration=100,
        loop=0,
    )
    output.seek(0)

    return output
