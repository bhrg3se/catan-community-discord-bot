import asyncio
import collections
import json
import re
import typing
from enum import Enum

from . import sheets


class GameKind(Enum):
    SEMIFINAL = "semifinal"
    FINAL = "final"
    LOSERS_FINAL = "losers_final"
    SHEET_ID = "1YGpofxuYdK1QfOgIhf5gUXEcZBNGTQptIyBGEnE_7aY"


GAME_LOCATIONS = [
    ((1, 1), GameKind.SEMIFINAL),
    ((9, 1), GameKind.SEMIFINAL),
    ((1, 18), GameKind.SEMIFINAL),
    ((9, 18), GameKind.SEMIFINAL),
    ((5, 9), GameKind.FINAL),
    # ((1, 27), GameKind.LOSERS_FINAL),
    # ((5, 27), GameKind.LOSERS_FINAL),
    # ((9, 27), GameKind.LOSERS_FINAL),
]


class PlayerInfo(typing.NamedTuple):
    discord_name: str
    colonist_name: str
    score: int
    win: bool
    played_semifinals: bool = True


class GameInfo(typing.NamedTuple):
    name: str
    page: int
    sheet_name: str
    document_name: str
    location: typing.Tuple[int, int]
    kind: GameKind
    player_info: typing.List[PlayerInfo]
    nrof_pages: int


def get_multiplier(game: GameInfo, player: PlayerInfo, settings: dict) -> int:
    if not player.played_semifinals:
        return 1
    print(settings["rank_bracket_multiplier"] ** (game.nrof_pages - game.page))
    return settings["rank_bracket_multiplier"] ** (game.nrof_pages - game.page)


def get_nrof_raffles(game: GameInfo, player: PlayerInfo) -> int:
    if game.page == 0 and player.win and game.kind == GameKind.FINAL:
        return 0  # The winner gets a guaranteed prize, no raffle
    if game.page >= 3 and not player.win and game.kind == GameKind.SEMIFINAL:
        return 0

    base_raffle = 16 >> game.page
    if not player.win:
        base_raffle >>= 1  # Divide by 2
    if game.kind == GameKind.SEMIFINAL:
        base_raffle >>= 1
    return base_raffle


async def iter_player_info(results_spreadsheet, score_page, position, points_to_win):
    for player_index in range(4):
        sheets.assert_cell_string(
            results_spreadsheet,
            score_page,
            (position[0], position[1] + 2 + player_index),
        )
        player_name = results_spreadsheet.get_value(
            score_page, position[0], position[1] + 2 + player_index
        )
        sheets.assert_cell_int(
            results_spreadsheet,
            score_page,
            (position[0] + 2, position[1] + 2 + player_index),
        )
        player_score = results_spreadsheet.get_value(
            score_page, position[0] + 2, position[1] + 2 + player_index
        )

        colonist_name = results_spreadsheet.get_value(
            score_page, position[0] + 1, position[1] + 2 + player_index
        )

        if player_score >= points_to_win:
            win = True
        else:
            win = False

        yield PlayerInfo(
            discord_name=player_name,
            colonist_name=colonist_name,
            score=player_score,
            win=win,
        )


async def iter_games_finals(sheet_id, config, points_to_win):
    results_spreadsheet = await sheets.load_sheet_contents(sheet_id, config)
    pages = results_spreadsheet.sheets
    # All pages that contain a number are considered ranking pages
    score_pages = [i for i in pages if re.search(r"\d+", i)]
    semifinals_players = set()

    document_name = results_spreadsheet.get_title()

    for index, score_page in enumerate(score_pages):
        for position, location_type in GAME_LOCATIONS:
            content = results_spreadsheet.get_value(
                score_page, position[0], position[1]
            )
            if not content:
                print(f"Missing table at {position}, skipping")

                if results_spreadsheet.get_value(
                    score_page, position[0], position[1] + 1
                ):
                    raise ValueError(
                        "It seems like the "
                        + str(location_type)
                        + " of "
                        + score_page
                        + "at "
                        + sheets.coords_to_cell(position)
                        + " "
                        "does not exist, yet the cell beneath has content.\n"
                        "Aborting in case this data was in the wrong cell."
                    )
                continue
            else:
                print(
                    f"Processing table on sheet {score_page!r} at {sheets.coords_to_cell(position)!r}: {content!r}"
                )

            player_info = []
            async for player in iter_player_info(
                results_spreadsheet=results_spreadsheet,
                points_to_win=points_to_win,
                score_page=score_page,
                position=position,
            ):
                if location_type == GameKind.SEMIFINAL:
                    played_in_semifinals = True
                    semifinals_players.add(player.discord_name)
                else:
                    played_in_semifinals = player.discord_name in semifinals_players

                player_info.append(
                    PlayerInfo(
                        player.discord_name,
                        player.colonist_name,
                        player.score,
                        player.win,
                        played_in_semifinals,
                    )
                )

            yield GameInfo(
                kind=location_type,
                name=content,
                location=position,
                page=index,
                player_info=player_info,
                sheet_name=score_page,
                document_name=document_name,
                nrof_pages=len(score_pages),
            )


async def compute_lp_for_tournament(
    sheet_id, config, points_to_win=10, settings: dict = {}
):
    player_scores: collections.Counter[str] = collections.Counter()
    colonist_names = {}
    document_name = None

    async for game in iter_games_finals(sheet_id, config, points_to_win):
        for player in game.player_info:
            lp = player.score * 10
            if game.kind == GameKind.FINAL and player.win:
                lp += settings["rank_bonus_win_final"]
            elif game.kind == GameKind.SEMIFINAL and player.win:
                lp += settings["rank_bonus_win_semifinal"]
            elif game.kind == GameKind.SEMIFINAL and not player.win:
                lp += settings["rank_bonus_sf_loser_bonus"]

            multiplier = get_multiplier(game, player, settings)

            player_scores[player.discord_name] += int(lp * multiplier)
            colonist_names[player.discord_name] = player.colonist_name

        document_name = game.document_name

    final_data = [
        (key, colonist_names.get(key), value) for key, value in player_scores.items()
    ]
    final_data.sort(key=lambda i: i[2])

    sheet_id = await sheets.edit_document(
        config,
        f"Scores {document_name!r}",
        [
            {
                "startRow": 0,
                "startColumn": 0,
                # "range": f"{sheets.coords_to_cell((0, 0))}:{sheets.coords_to_cell((2, len(player_scores)))}",
                "rowData": [
                    {
                        "values": [
                            sheets.create_cell_data("Discord Name"),
                            sheets.create_cell_data("Colonist Name"),
                            sheets.create_cell_data("lp"),
                        ]
                    },
                    *[
                        {
                            "values": [
                                sheets.create_cell_data(i[0]),
                                sheets.create_cell_data(i[1]),
                                sheets.create_cell_data(i[2]),
                            ]
                        }
                        for i in final_data
                    ],
                ],
            }
        ],
    )

    return sheet_id


async def compute_raffle_for_tournament(sheet_id, config, vp_to_win):
    raffle_counter = collections.Counter()

    async for game in iter_games_finals(sheet_id, config, vp_to_win):
        for player in game.player_info:
            if game.kind == GameKind.SEMIFINAL and player.win:
                continue
            nrof_raffles = get_nrof_raffles(game, player)
            raffle_counter[player.discord_name] += nrof_raffles

    output = []
    for key, value in raffle_counter.items():
        output.extend(
            [key.replace(" ", "")] * value,
        )
    return " ".join(output)


if __name__ == "__main__":
    with open("../../secret.json") as f:
        config = json.load(f)

    print(asyncio.get_event_loop())
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    # dat = asyncio.run(create_table_sheets(config, "alpha", tables))
    # asyncio.run(compute_lp_for_tournament(SHEET_ID, config))
