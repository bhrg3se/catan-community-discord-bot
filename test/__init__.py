import discord


class FakeClient(discord.Client):
    def __init__(self, *args, **kwargs):
        # Don't call the constructor of super on purpose
        pass

    def run(self, token):
        self.on_ready()
