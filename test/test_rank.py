from unittest import mock
from src.commands import rank_commands
from asyncio.futures import Future
import pytest


async def resolved_future(value):
    return value


@pytest.mark.asyncio
async def test_rank():
    message = mock.Mock()
    message.content = "!rank"
    message.mentions = []
    message.channel.send.return_value=resolved_future(None)

    state = mock.Mock()
    state.get_user_by_name = mock.Mock(return_value=resolved_future({
        "display_name": "mouse1234",
        "discord_username": "<@:12345>"
    }))
    state.db = mock.MagicMock()
    state.db["user_lp"].aggregate.return_value.to_list = mock.Mock(return_value=resolved_future(
        [{"lp": 5, "matches": 3, "normalized_wins": 4}]
    ))
    state.get_users_ranked_above = mock.Mock(return_value=resolved_future(5))
    state.get_next_ranked_user.return_value = resolved_future({
        "user_data": [{
            "display_name": "mouse1234",
        }],
        "lp": 12,
    })
    state.get_pronoun.return_value = 'he', 'him'
    state.settings = {
        "link": "https://colonist.io"
    }

    await rank_commands.rank(
        state,
        message
    )
