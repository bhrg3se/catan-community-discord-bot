import asyncio

import db


async def set_or_create_value(
        database,
        filter,
        values
):
    element = await database["user_lp"].find_one(
        filter
    )

    if element is not None:
        return False
    else:
        val = {
            **filter,
            **values
        }
        await database["user_lp"].insert_one(val)
        return True


async def migrate_user_db():
    database = db.get_db(True)
    if "user_lp" not in await database.list_collection_names() or True:
        await database.drop_collection("user_lp")
        await database.create_collection(
            "user_lp",
            validator={
                "$jsonSchema": {
                    "bsonType": "object",
                    "required": [
                        "user",
                        "year",
                        "month",
                        "type",
                        "map",
                        "nrof_players",
                        "lp",
                        "matches",
                        "normalized_wins"
                    ],
                    "properties": {
                        "user": {
                            "bsonType": "objectId",
                            "description": "user must reference a user"
                        },
                        "year": {
                            "bsonType": "int",
                            "description": "year must be a integer"
                        },
                        "month": {
                            "bsonType": "int",
                            "description": "month must be a integer"
                        },
                        "type": {
                            "bsonType": "string",
                            "pattern": "mm|tourney",
                            "description": "mm must be 'mm' or 'tourney'"
                        },
                        "map": {
                            "bsonType": "string",
                            "pattern": "base",
                            "description": "map must be base"
                        },
                        "nrof_players": {
                            "bsonType": "int",
                            "description": "nrof_players must be a integer"
                        },
                        "lp": {
                            "bsonType": "int",
                            "description": "lp must be a integer"
                        },
                        "matches": {
                            "bsonType": "int",
                            "description": "matches must be a integer"
                        },
                        "normalized_wins": {
                            "bsonType": "int",
                            "description": "normalized_wins must be a integer"
                        }
                    }
                }
            }
        )

    async for user in database["users"].find({}):
        for year, months in user["history"].items():
            for month, data in months.items():
                if not isinstance(data, dict):
                    print("Expected data to be a dict")
                    continue
                if not await set_or_create_value(database, {
                    "user": user["_id"],
                    "year": int(year),
                    "month": int(month),
                    "type": "mm",
                    "mode": "base",
                    "map": "base",
                    "nrof_players": 4
                }, data):
                    print(f"Value already exists for user {user.get('display_name')=}")
        for year, months in user.get("tournaments", {}).items():
            if not isinstance(months, dict):
                continue
            for month, tourneys in months.items():
                if not isinstance(tourneys, dict):
                    continue
                for tourney, lp in tourneys.items():
                    tournament = await database["tournaments"].find_one({
                        "name": tourney
                    })

                    if tournament is None:
                        tournament = await database["tournaments"].insert_one(
                            {
                                "month": month,
                                "year": year,
                                "name": tourney
                            }
                        )
                        print("Cannot find a tournament for ", tourney)
                        continue

                    if not await set_or_create_value(
                            database, {
                                "user": user["_id"],
                                "year": int(year),
                                "month": int(month),
                                "type": "tourney",
                                "map": "base",
                                "mode": "base",
                                "nrof_players": 4,
                                "tournament": tournament["_id"]
                            },
                            {
                                "lp": lp,
                                "matches": 0,
                                "normalized_wins": 0
                            }):
                        print(f"Value already added")
                    else:
                        await database["user_lp"].update_one(
                            {
                                "user": user["_id"],
                                "year": int(year),
                                "month": int(month),
                                "type": "tourney",
                                "map": "base",
                                "mode": "base",
                                "nrof_players": 4
                            },
                            {
                                "$inc": {
                                    "lp": -lp
                                }
                            }
                        )


if __name__ == "__main__":
    asyncio.run(migrate_user_db())
