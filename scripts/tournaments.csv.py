import asyncio

import db


async def add_tournaments():
    tournaments = {
        "2021": {
            "1": [
                {
                    "name": "Open 11",
                    "path": "1.open11"},
                {
                    "name": "Open 12",
                    "path": "1.open12"},
                {
                    "name": "Open 13",
                    "path": "1.open13"},
                {
                    "name": "Open 14",
                    "path": "1.open14"
                }

            ],
            "2": [
                {
                    "name": "Open 15",
                    "path": "2021.2.Open15"},
                {
                    "name": "Open 16",
                    "path": "2021.2.Open16"},
                {
                    "name": "Valentines 1v1",
                    "path": "2021.2.Valentine1Vs1"}
            ]
        }
    }

    database = db.get_db(True)
    collection = database["tournaments"]
    for year, tourneys_year in tournaments.items():
        for month, tourneys_month in tourneys_year.items():
            for tourney in tourneys_month:
                result = await collection.update_many(
                    {
                        "name": tourney["name"]
                    },
                    {
                        "$set": {
                            "month": month,
                            "year": year,
                            "name": tourney["name"],
                            "path": tourney["path"],
                            "season": 2
                        }
                    },
                    upsert=True
                )
                print(year, month, tourney["name"], result.matched_count, result.modified_count, result.upserted_id)


if __name__ == "__main__":
    asyncio.run(add_tournaments())
